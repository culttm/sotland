var gulp = require('gulp'),
	jade = require('gulp-jade'),
	del = require('del'),
    fs = require('fs'),
    bowerFiles = require('main-bower-files'),
    uglify = require('gulp-uglify'),
    bower = require('gulp-bower'),
    gulpFilter = require('gulp-filter'),
    concat = require('gulp-concat'),
    compass = require('gulp-compass'),
    plumber = require('gulp-plumber'),
    changed = require('gulp-changed'), 
    notify     = require('gulp-notify'), 
    imagemin = require('gulp-imagemin'),
    coffee = require('gulp-coffee'),      
    sourcemaps = require('gulp-sourcemaps'),      
    pixrem = require('gulp-pixrem'),      
    browserSync = require('browser-sync'),
    // json = require('./src/json/data.json'),
    reload      = browserSync.reload,
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer-core'),
    mqpacker = require('css-mqpacker'),
    csswring = require('csswring');

 



// Gulp plumber error handler
var onError = function(err) {
    console.log(err);
};



// clear dist
gulp.task('clean', function (cb) {
    del([
        './dist/*.*'
      ], cb);
});


// clear
gulp.task('cleanImages', function (cb) {
    del([
        './dist/images/*.*'
      ], cb);
});

// clear
gulp.task('cleanAssets', function (cb) {
    del([
        './dist/assets/*'
      ], cb);
});


// clear
gulp.task('cleanFonts', function (cb) {
	del([
	    './dist/fonts/*'
	  ], cb);
});


// move assets folder
gulp.task('moveFonts', ['cleanFonts'],  function () {
    return  gulp.src('./src/fonts/**/*')
            .pipe(gulp.dest('./dist/fonts'));
});

// Compress and minify images to reduce their file size
gulp.task('moveImages', ['cleanImages'], function() {
    var imgSrc = './src/images/*.*',
        imgDst = './dist/images';
 
    return  gulp.src(imgSrc)
            .pipe(plumber({
                errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
            .pipe(changed(imgDst))
            .pipe(imagemin())
            .pipe(gulp.dest(imgDst));
});





// Compress and minify images to reduce their file size
gulp.task('moveAssets', ['cleanAssets'], function() {
    var imgSrc = './src/assets/**/*',
        imgDst = './dist/assets/';
 
    return  gulp.src(imgSrc)
            .pipe(plumber({
                errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
            .pipe(changed(imgDst))
            .pipe(imagemin())
            .pipe(gulp.dest(imgDst));
});






// bower
gulp.task('bower', function() {
    
  var jsFilter = gulpFilter('**/*.js');
  var cssFilter = gulpFilter('**/*.css');

      bower();

      if (fs.existsSync('./src/vendors')) { 
        return  gulp.src(bowerFiles())
                    .pipe(plumber({
                        errorHandler: function (error) {
                        console.log(error.message);
                        this.emit('end');
                    }}))        
                    .pipe(jsFilter)
                    .pipe(concat('libs.js'))
                    // .pipe(uglify())
                    .on('error', function(err) {
                        console.log(err)
                    })                    
                    .pipe(gulp.dest('./dist/js'))
                    .pipe(jsFilter.restore())    
                    .pipe(cssFilter)
                    .pipe(concat('libs.css'))
                    .on('error', function(err) {
                        console.log(err)
                    })                      
                    .pipe(gulp.dest('./dist/css'))
                    .pipe(cssFilter.restore())

      } 

});



// sass compass
gulp.task('compass', function() {
    var processors = [
        autoprefixer({browsers: ["last 1 version", "> 1%", "ie 8", "ie 7"]}),
        mqpacker,
        csswring
    ];

    return  gulp.src('./src/sass/**/*.scss')
            .pipe(plumber({
                errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
            .pipe(compass({
                config_file: './config.rb',
                css: './src/css',
                sass: './src/sass'
            }))
            .on('error', function(err) {
                console.log(err)
            })
            .pipe(pixrem())
            .pipe(postcss(processors))
            .pipe(gulp.dest('./dist/css/'))
            .pipe(notify({ message: 'Compass task complete' }));            
});

 
 

// jade to html
gulp.task('jade', function () {
    return  gulp.src('./src/jade/*.jade')
            .pipe(plumber({
                errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))    
            .pipe(jade({
                'pretty': true,
                'locals': /*json*/''
            }))
            .on('error', function(err) {
                console.log(err)
            })            
            .pipe(gulp.dest('./dist'))
            .pipe(notify({ message: 'Jade task complete' }));
});




gulp.task('coffee', function() {
  gulp.src('./src/coffee/*.coffee')
    .pipe(plumber({
        errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    // .pipe(sourcemaps.init())   
    .pipe(coffee({bare: true}))
    // .pipe(sourcemaps.write('./'))
    .on('error', function(err) {
        console.log(err)
    })      
    .pipe(concat('main.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('./dist/js/'))
    .pipe(notify({ message: 'Coffee task complete' }))
});




// Static server
gulp.task('serve', ['compass', 'jade', 'coffee',  'moveImages', 'moveAssets', 'moveFonts'], function() {
    browserSync({
        server: "./dist"
    });

    // gulp.watch("dist/*.html").on('change', reload);
    // gulp.watch("dist/css/*.css").on('change', reload);
    // gulp.watch("dist/js/*.js").on('change', reload);
});


 

gulp.task('watch', ['clean', 'moveFonts', 'compass', 'bower', 'jade', 'moveAssets', 'moveImages', 'coffee', 'serve'], function(){
    gulp.watch('./src/assets/**/*', ['moveAssets']);
    gulp.watch('./src/images/**/*', ['moveImages']);
    gulp.watch('./src/jade/**/*.jade', ['jade']);
    gulp.watch('./src/sass/**/*.scss', ['compass']);
    gulp.watch('./src/coffee/*.coffee', ['coffee']);
	gulp.watch('bower.json', ['bower']);
});