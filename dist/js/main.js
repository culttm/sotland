
/* 
 *   Init owlCarousel with custom DOM options
 *   https://github.com/smashingboxes/OwlCarousel2
 */
+(function($, window) {
  'use strict';
  var CreateCarousel, Plugin;
  CreateCarousel = (function() {
    function CreateCarousel(element) {
      this.el = $(element);
      this.options = {
        loop: true,
        nav: true
      };
    }

    CreateCarousel.prototype.build = function() {
      var $this, opt;
      $this = this.el;
      opt = this.extendOptions();
      if (typeof $.fn.owlCarousel === 'function') {
        return $this.owlCarousel(opt);
      } else {
        return console.log('You must install owlCarousel\nhttps://github.com/smashingboxes/OwlCarousel2');
      }
    };

    CreateCarousel.prototype.getOptions = function() {
      var $this, domOptions;
      $this = this.el;
      return domOptions = $this.data('options');
    };

    CreateCarousel.prototype.extendOptions = function() {
      var $domOptions, $extendedOptions, $options, $this;
      $this = this.el;
      $options = this.options;
      $domOptions = this.getOptions();
      return $extendedOptions = $.extend($options, $domOptions);
    };

    return CreateCarousel;

  })();
  Plugin = function(option) {
    return this.each(function() {
      var $this, data;
      $this = $(this);
      data = $this.data('cl.createcarousel');
      if (!data) {
        $this.data('cl.createcarousel', (data = new CreateCarousel(this)));
      }
      if (typeof option === 'string') {
        data[option]();
      }
    });
  };
  $(function() {
    return $('.carousel').each(function() {
      return Plugin.call($(this), 'build');
    });
  });
})(jQuery, window);


/*
 * Create custom autocomlete initialization https://jqueryui.com/autocomplete/
 * param: source -> searchDummiesData
 */
$(function() {
  $('#search-input').autocomplete({
    source: searchDummiesData,
    appendTo: "#autocmplete-result-search-panel",
    open: function(event, ui) {
      var data, widget, wrapper;
      data = $(this).data('ui-autocomplete');
      widget = $(data.menu.element);
      wrapper = widget.closest('.autocmplete-results');
      wrapper.removeClass('hide').addClass('show');
      widget.addClass('custom-list').removeAttr('style');
    },
    close: function(event, ui) {
      var data, widget, wrapper;
      data = $(this).data('ui-autocomplete');
      widget = $(data.menu.element);
      wrapper = widget.closest('.autocmplete-results');
      wrapper.addClass('hide').removeClass('show');
      widget.removeAttr('style');
    },
    create: function() {
      return $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
        return $('<li class="item"></li>').append("<div data-label='" + item.label + "' class='item-thumb'><img class='img-responsive' src='" + item.image + "'></div>").append("<div class='item-description'>" + item.description + "</div>").append("<div class='item-cost'><div class='costs'><div class='cost-1'>" + item.costFirst + "</div><div class='cost-2'><span>VIP</span>" + item.costSecond + "</div></div></div>").appendTo(ul);
      };
    }
  });
});


/* INIT PLUGINS */
$(function() {

  /*
   * FancySelect (http://code.octopuscreative.com/fancyselect/)
   */
  var scrollbarWidth;
  $('.form-elements select').fancySelect({
    includeBlank: true
  });

  /* 
   * [jQuery Validation Plugin](http://jqueryvalidation.org/)
   */
  $.extend($.validator.messages, {
    required: 'Заполните это поле!',
    email: 'Неверный формат!'
  });
  $.extend($.validator.rules, {
    select: 'required'
  });
  $('form.validatable').each(function() {
    return $(this).validate();
  });

  /*
   * HTML5 Placeholder jQuery Plugin (https://github.com/mathiasbynens/jquery-placeholder)
   */
  $('.form-elements input, .form-elements textarea').placeholder();

  /*
   * Masked Input Plugin for jQuery https://github.com/digitalBush/jquery.maskedinput
   */
  $(".phone-input").mask("+9 (999) 999-99-99");

  /*
   * Fix modal right offset
   */
  (scrollbarWidth = function() {
    var child, parent, width;
    parent = $('<div style="width:50px;height:50px;overflow:auto"><div><div/></div>').appendTo('body');
    child = parent.children();
    width = child.innerWidth() - child.height(99).innerWidth();
    parent.remove();
    $('head').append('<style>.modal-open-noscroll{margin-right: ' + width + 'px ;}</style>');
    return width;
  })();
  $('.modal').on('show.bs.modal', function() {
    if ($(document).height() > $(window).height()) {
      return $('body').addClass('modal-open-noscroll');
    } else {
      return $('body').removeClass('modal-open-noscroll');
    }
  });
  $('.modal').on('hide.bs.modal', function() {
    return $('body').removeClass('modal-open-noscroll');
  });
});
