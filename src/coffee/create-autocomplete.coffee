###
# Create custom autocomlete initialization https://jqueryui.com/autocomplete/
# param: source -> searchDummiesData
###
$ ->
    $('#search-input').autocomplete({
        
        source: searchDummiesData

        appendTo: "#autocmplete-result-search-panel"

        open: (event, ui) ->
            data = $(this).data 'ui-autocomplete'
            widget = $(data.menu.element)
            wrapper = widget.closest '.autocmplete-results'

            wrapper
                .removeClass 'hide'
                .addClass 'show'

            widget
                .addClass 'custom-list'
                .removeAttr 'style'
            return

        close: (event, ui) ->
            data = $(this).data 'ui-autocomplete'
            widget = $(data.menu.element)
            wrapper = widget.closest '.autocmplete-results'

            wrapper
                .addClass 'hide'
                .removeClass 'show'

            widget
                .removeAttr 'style'
            return

        create: ->
            $(this).data('ui-autocomplete')
                ._renderItem = (ul, item) ->
                    $('<li class="item"></li>')
                        .append "<div data-label='#{item.label}' class='item-thumb'><img class='img-responsive' src='#{item.image}'></div>"
                        .append "<div class='item-description'>#{item.description}</div>"
                        .append "<div class='item-cost'><div class='costs'><div class='cost-1'>#{item.costFirst}</div><div class='cost-2'><span>VIP</span>#{item.costSecond}</div></div></div>"
                        .appendTo ul
                        
    })

    return