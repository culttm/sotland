### INIT PLUGINS ###
$ ->
    ###
    # FancySelect (http://code.octopuscreative.com/fancyselect/)
    ###
    $('.form-elements select').fancySelect({
        includeBlank: true
    })

    ### 
    # [jQuery Validation Plugin](http://jqueryvalidation.org/) 
    ###
    $.extend($.validator.messages, {
        required: 'Заполните это поле!'
        email: 'Неверный формат!'
    })
    
    $.extend($.validator.rules, {
        select: 'required'
    })


    $('form.validatable').each ->
        $(@).validate()

    ###
    # HTML5 Placeholder jQuery Plugin (https://github.com/mathiasbynens/jquery-placeholder)
    ###
    $('.form-elements input, .form-elements textarea').placeholder()

    ###
    # Masked Input Plugin for jQuery https://github.com/digitalBush/jquery.maskedinput
    ###
    $(".phone-input").mask("+9 (999) 999-99-99");


    ###
    # Fix modal right offset
    ###

    do scrollbarWidth = () ->
        parent = $('<div style="width:50px;height:50px;overflow:auto"><div><div/></div>').appendTo('body')
        child = parent.children()
        width = child.innerWidth() - child.height(99).innerWidth()
        parent.remove()
        $('head').append('<style>.modal-open-noscroll{margin-right: '+width+'px ;}</style>')
        width

    $('.modal').on 'show.bs.modal', ->
        if $(document).height() > $(window).height()
            $('body')
                .addClass 'modal-open-noscroll'
        else
            $('body')
                .removeClass 'modal-open-noscroll'

    $('.modal').on 'hide.bs.modal', ->
        $('body')
            .removeClass 'modal-open-noscroll'

    return