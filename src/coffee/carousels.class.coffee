### 
#   Init owlCarousel with custom DOM options
#   https://github.com/smashingboxes/OwlCarousel2
###
+ do ($ = jQuery, window) ->
    'use strict';
    class CreateCarousel
        constructor: (element) ->
            @.el = $(element)
            @.options = {
                loop: true
                nav: true
            }

        build: ->
            $this = @.el
            opt = @.extendOptions()

            if typeof $.fn.owlCarousel == 'function'
                $this.owlCarousel opt
            else
                console.log 'You must install owlCarousel\nhttps://github.com/smashingboxes/OwlCarousel2'

        getOptions: ->
            $this = @.el
            domOptions = $this.data('options')

        extendOptions: ->
            $this = @.el
            $options = @.options
            $domOptions = @.getOptions()

            $extendedOptions = $.extend($options, $domOptions)

    
    Plugin = (option) ->
        @.each ->
            $this = $(@)
            data = $this.data('cl.createcarousel')

            if !data 
                $this.data('cl.createcarousel', (data = new CreateCarousel @))
            if (typeof option == 'string')
                data[option]()
            return

    $ ->
        $('.carousel').each ->
            Plugin.call($(@), 'build')
    return